//
//  main.cpp
//  HW3 P1
//
//  Created by Alyssa Maligaya on 11/13/20.
//

#include <iostream>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::setprecision;
using std::showpoint;

//Function Celsius
double celsius(const int tempF)
{
    double tempCelsius;
    tempCelsius = (5.0 / 9.0) * (tempF - 32);
    return tempCelsius;
}

//Function Fahrenheit
double fahrenheit(const int tempC)
{
    double tempFahrenheit;
    tempFahrenheit = (tempC * 9.0 / 5.0) + 32;
    return tempFahrenheit;
}

//Main function to prompt user for input and display results
int main()
{
    double tempC, tempF, ansCelsius, ansFahrenheit;
    cout << "Enter temperature in Fahrenheit to be converted to Celsius: ";
    cin >> tempF;
    //Call Celsius function and display result
    ansCelsius = celsius(tempF);
    cout << setprecision(4);
    cout << tempF << " is equivalent to " << showpoint << ansCelsius << " in Celsius. \n" << endl;
    cout << "Enter temperature in Celsius to be converted to Fahrenheit: ";
    cin >> tempC;
    //Call Fahrenheit function and display result
    ansFahrenheit = fahrenheit(tempC);
    cout << tempC << " is equivalent to " << showpoint << ansFahrenheit << " in Fahrenheit. \n" << endl;
    
    return 0;
}
